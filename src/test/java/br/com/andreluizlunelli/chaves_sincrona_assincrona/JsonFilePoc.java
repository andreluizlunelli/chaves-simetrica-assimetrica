package br.com.andreluizlunelli.chaves_sincrona_assincrona;

import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Test;

public class JsonFilePoc {
	
	@Test
	public void test01() {
		JSONObject jo = new JSONObject();
		JSONArray arrayPessoas = new JSONArray();
		arrayPessoas.add("andre");
		arrayPessoas.add("wiu");
		arrayPessoas.add("luiz");
		jo.put("pessoas", arrayPessoas);
		try (FileWriter file = new FileWriter("config.json")) {
			file.write(jo.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + jo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
