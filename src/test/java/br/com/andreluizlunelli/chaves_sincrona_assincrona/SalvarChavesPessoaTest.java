package br.com.andreluizlunelli.chaves_sincrona_assincrona;

import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;

import org.json.simple.JSONObject;
import org.junit.Test;

import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavepublica.RepositorioChavePublica;

public class SalvarChavesPessoaTest {
	
	@Test
	public void test01() {
		RepositorioChavePublica repo = new RepositorioChavePublica();
		Pessoa pessoa1 = new Pessoa("andre", repo);
		repo.addPessoa(pessoa1);
		Pessoa pessoa2 = new Pessoa("willian", repo);
		repo.addPessoa(pessoa2);
		Pessoa pessoa3 = new Pessoa("luiz", repo);
		repo.addPessoa(pessoa3);
		
		
		pessoa1.mandaMensagemPara(pessoa2, "opa blz? c já perguntou q dia tem que entregar esse trabalho?");
		pessoa2.mandaMensagemPara(pessoa1, "ncei vei, dxa eu ve c ele");
		pessoa2.mandaMensagemPara(pessoa3, "fessor, q dia tem que entrega o trabalho?");
		
		SalvarChavesPessoa salvarChavesPessoa = new SalvarChavesPessoa(pessoa1);
		salvarChavesPessoa.salvar();
		JSONObject json = salvarChavesPessoa.ler();
		System.out.println("\nJSON Object: " + json);
		
	}
}
