package br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavepublica;

import java.security.PublicKey;

public class PessoaRepositorio {
	String nome;
	PublicKey chavePublica;
	
	public PessoaRepositorio(String nome, PublicKey chavePublica) {
		this.nome = nome;
		this.chavePublica = chavePublica;
	}

	public String getNome() {
		return nome;
	}

	public PublicKey getChavePublica() {
		return chavePublica;
	}
		
}
