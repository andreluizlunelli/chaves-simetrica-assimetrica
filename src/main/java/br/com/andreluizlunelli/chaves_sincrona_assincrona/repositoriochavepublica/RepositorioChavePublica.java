package br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavepublica;

import java.security.PublicKey;
import java.util.HashMap;

import br.com.andreluizlunelli.chaves_sincrona_assincrona.Pessoa;

public class RepositorioChavePublica {

	HashMap<String, PessoaRepositorio> contatos = new HashMap<String, PessoaRepositorio>();
	
	public void addPessoa(Pessoa pessoa) {		
		getContatos().put(pessoa.getNome(), new PessoaRepositorio(pessoa.getNome(), pessoa.getChavePublica()));
	}

	public HashMap<String, PessoaRepositorio> getContatos() {
		return contatos;
	}

	public void setContatos(HashMap<String, PessoaRepositorio> contatos) {
		this.contatos = contatos;
	}
	
	public PublicKey getPublicKey(String nome) {
		return (getContatos().get(nome)).getChavePublica();
	}

}
