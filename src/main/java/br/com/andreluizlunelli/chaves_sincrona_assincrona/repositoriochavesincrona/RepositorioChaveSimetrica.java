package br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona;

import java.util.HashMap;
import javax.crypto.SecretKey;
import br.com.andreluizlunelli.chaves_sincrona_assincrona.Pessoa;

public class RepositorioChaveSimetrica {
	HashMap<String, PessoaChaveSimetrica> repo = new HashMap<String, PessoaChaveSimetrica>();

	public boolean tenhoChaveSimentricaParaConversa(String nomePessoa) {
		return (repo.get(nomePessoa) == null) ? false : true ;
	}

	public void addChaveSimetrica(Pessoa para, String initVector, SecretKey key) {
		repo.put(para.getNome(), new PessoaChaveSimetrica(para.getNome(), initVector, key));
	}
	
	public PessoaChaveSimetrica getChaveSimetrica(String nome) {
		return repo.get(nome);
	}
	
	public HashMap<String, PessoaChaveSimetrica> getRepo() {
		return repo;
	}
}
