package br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona;

import javax.crypto.SecretKey;

public class PessoaChaveSimetrica {
	String nome;
	private String AESInitVector; 
	private SecretKey AESKey;
	
	public PessoaChaveSimetrica(String nome, String aESInitVector, SecretKey aESKey) {
		super();
		this.nome = nome;
		AESInitVector = aESInitVector;
		AESKey = aESKey;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getAESInitVector() {
		return AESInitVector;
	}

	public void setAESInitVector(String aESInitVector) {
		AESInitVector = aESInitVector;
	}

	public SecretKey getAESKey() {
		return AESKey;
	}

	public void setAESKey(SecretKey aESKey) {
		AESKey = aESKey;
	}	
	
	
}
