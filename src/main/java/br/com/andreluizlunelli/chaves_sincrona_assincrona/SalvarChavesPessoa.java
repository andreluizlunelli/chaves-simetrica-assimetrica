package br.com.andreluizlunelli.chaves_sincrona_assincrona;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavepublica.RepositorioChavePublica;
import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona.PessoaChaveSimetrica;
import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona.RepositorioChaveSimetrica;

public class SalvarChavesPessoa {

	private Pessoa pessoa;

	public SalvarChavesPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void salvar() {
		JSONObject json = obterJson();
		try (FileWriter file = new FileWriter(".dercygonsalves")) {					
			file.write(new String(xorWithKey(json.toString().getBytes(), pessoa.getNome().getBytes()), "UTF-8"));
			System.out.println("Successfully Copied JSON Object to File...");			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public JSONObject ler() {		
		File file = new File(".dercygonsalves");
		String lido = null;
		JSONObject json = null;
		try {
			lido = new String(xorWithKey(Files.readAllBytes(file.toPath()), pessoa.getNome().getBytes()), "UTF-8");
			JSONParser parser = new JSONParser();
			json = (JSONObject) parser.parse(lido);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return json;
	}

	private byte[] xorWithKey(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i%key.length]);
        }
        return out;
    }
	
	public JSONObject obterJson() {
		PrivateKey chavePrivada = pessoa.chavePrivada;
		PublicKey chavePublica = pessoa.chavePublica;
		RepositorioChaveSimetrica repoSimetrica = pessoa.repoSimetrica;
		
		JSONObject joMain = new JSONObject();
		JSONObject joPessoa = new JSONObject();		
		JSONArray listaChavesSimetricas = getListSimetricas(pessoa.repoSimetrica.getRepo());					
		joPessoa.put("nome", pessoa.nome);
		joPessoa.put("publica", Base64.getEncoder().encodeToString(chavePublica.toString().getBytes()));
		joPessoa.put("privada", Base64.getEncoder().encodeToString(chavePrivada.toString().getBytes()));
		joPessoa.put("chaves_simetricas", listaChavesSimetricas);
		joMain.put("pessoa", joPessoa);
		return joMain;
	}
	
	public JSONArray getListSimetricas(HashMap<String, PessoaChaveSimetrica> hashMap) {
		JSONArray _return = new JSONArray();
		Iterator<Entry<String, PessoaChaveSimetrica>> it = hashMap.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry)it.next();
	        String key = (String) pair.getKey();
	        PessoaChaveSimetrica value = (PessoaChaveSimetrica) pair.getValue();	        
	        _return.add(obterObjetoDeChaveSimetrica(value));
	    }
		return _return;
	}
	
	private JSONObject obterObjetoDeChaveSimetrica(PessoaChaveSimetrica pcs) {
		JSONObject j = new JSONObject();		
		j.put("nome", pcs.getNome());
		j.put("aesKey", AES.encodeAesKey(pcs.getAESKey()));
		j.put("initVector", pcs.getAESInitVector());
		return j;
	}

}
