package br.com.andreluizlunelli.chaves_sincrona_assincrona;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.SignedObject;
import java.util.concurrent.ThreadLocalRandom;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;

import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavepublica.RepositorioChavePublica;
import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona.PessoaChaveSimetrica;
import br.com.andreluizlunelli.chaves_sincrona_assincrona.repositoriochavesincrona.RepositorioChaveSimetrica;

public class Pessoa {
	String nome;
	PrivateKey chavePrivada;
	PublicKey chavePublica;
	RepositorioChaveSimetrica repoSimetrica = new RepositorioChaveSimetrica();
	RepositorioChavePublica repoBuscaChavesPublicas = new RepositorioChavePublica();
	
	public Pessoa(String nome, RepositorioChavePublica repo) {
		super();
		this.nome = nome;
		this.repoBuscaChavesPublicas = repo;
		gerarChavesCriptografiaAssimetrica();
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public PublicKey getChavePublica() {
		return chavePublica;
	}
	
	public void gerarChavesCriptografiaAssimetrica() {
		try {
			
			KeyPair pair = KeyPairGenerator.getInstance("RSA").generateKeyPair();			
			chavePrivada = pair.getPrivate();
			chavePublica = pair.getPublic();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void mandaMensagemPara(Pessoa pessoa, String mensagem) {
		if (! repoSimetrica.tenhoChaveSimentricaParaConversa(pessoa.getNome())) {
			// tem que criar
			enviarChaveSimetrica(pessoa);
		}
		// manda a mensagem normal	
		PessoaChaveSimetrica simetrica = repoSimetrica.getChaveSimetrica(pessoa.getNome());		
		pessoa.receberMensagem(this, AES.encrypt(simetrica.getAESKey(), simetrica.getAESInitVector(), mensagem));
	}
	
	public void receberMensagem(Pessoa de, String encrypt) {
		PessoaChaveSimetrica simetrica = repoSimetrica.getChaveSimetrica(de.getNome());
		String ultimaMensagemRecebidaDescriptografada = AES.decrypt(simetrica.getAESKey(), simetrica.getAESInitVector(), encrypt);
		System.out.println(String.format("[MENSAGEM DE %s PARA %s] ", de.getNome(), this.getNome()));
		System.out.println(String.format("[MENSAGEM RECEBIDA CRIPTOGRAFADA] %s", encrypt));
		System.out.println(String.format("[MENSAGEM RECEBIDA desCRIPTOGRAFADA] %s", ultimaMensagemRecebidaDescriptografada));
	}			

	private void enviarChaveSimetrica(Pessoa para) {		
		System.out.println(String.format("[CRIANDO CHAVE AES] %s enviando >>> %s", getNome(), para.getNome()));
		Cipher encCipher;
		try {			
			
			// metodo para iniciar a conversa o ator que tem que criar a chave RSA pra iniciar a conversa
			// e um metodo para receber a mensagem (passo de inicialização no ator)
			
			KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
			keyGenerator.init(128);
			
			SecretKey key = keyGenerator.generateKey();
			String randomNumInitVector = String.valueOf(ThreadLocalRandom.current().nextInt(0, 10));
			String initVector = AES.sha1(randomNumInitVector).substring(0, 16);			
			String enviar = AES.encodeAesKey(key)+":"+initVector;
			
			// autenticidade
			Signature signature = Signature.getInstance("SHA1withRSA");
			signature.initSign(this.chavePrivada, new SecureRandom());
			signature.update(enviar.getBytes());
			//
	        
			encCipher = Cipher.getInstance("RSA");
			encCipher.init(Cipher.ENCRYPT_MODE, para.getChavePublica());
						
//			SealedObject encriptMessage = new SealedObject(enviar, encCipher);
			SignedObject encriptMessage = new SignedObject(enviar, this.chavePrivada, signature);
						
			repoSimetrica.addChaveSimetrica(para, initVector, key);

			para.receberChaveSimetrica(this, encriptMessage);
			
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			e.printStackTrace();
		}		
	}

	private void receberChaveSimetrica(Pessoa pessoaQueEnviou, SignedObject signedMessage) {
		Cipher decCipher;
		try {
			Signature signature = Signature.getInstance("SHA1withRSA");
			boolean isAutentico = signedMessage.verify(pessoaQueEnviou.getChavePublica(), signature);
			System.out.println("Mensagem é autentica? "+((isAutentico) ? "Sim" : "Não"));
			
			decCipher = Cipher.getInstance("RSA");
			decCipher.init(Cipher.DECRYPT_MODE, this.chavePrivada);

			String mensagemFinal = (String) signedMessage.getObject();
			
			String[] pair = mensagemFinal.split(":");
			SecretKey key = AES.decodeAesKey(pair[0]);
			String initVector = pair[1];

			repoSimetrica.addChaveSimetrica(pessoaQueEnviou, initVector, key);
			
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			e.printStackTrace();
		} catch (InvalidKeyException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SignatureException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public PublicKey devolveChavePublicaProSerumaninho(String nome) {
		return repoBuscaChavesPublicas.getPublicKey(nome);
	}

}
